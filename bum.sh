rm -r ./bin ./src
mkdir bin
mkdir src

echo 'public class test {'                           > src/test.java
echo   'public static void main(String[] args) {'   >> src/test.java
echo     'System.out.println("Hello World!");'      >> src/test.java
echo   '}'                                          >> src/test.java
echo '}'                                            >> src/test.java


javac -d bin src/test.java
java -classpath ./bin test

cd bin
echo main-class: test>manifest.mf
jar -cmf manifest.mf test.jar .

#run jar to next script. 


 
